package li.doerf.zueriluft.db.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "locations")
data class Location(@PrimaryKey(autoGenerate = true) var id: Long?,
                    @ColumnInfo(name="name") var name: String,
                    @ColumnInfo(name="latitude") var latitude: Double?,
                    @ColumnInfo(name="longitude") var longitude: Double?,
                    @ColumnInfo(name="remoteId") var remoteId: Long
) : Parcelable
