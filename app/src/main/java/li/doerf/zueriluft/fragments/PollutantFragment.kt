package li.doerf.zueriluft.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import li.doerf.zueriluft.R
import li.doerf.zueriluft.activities.DetailsActivity
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.db.entities.Pollutant
import li.doerf.zueriluft.db.entities.Value
import li.doerf.zueriluft.util.PollutantUtils
import li.doerf.zueriluft.util.dateAsDate
import java.util.*


class PollutantFragment : Fragment() {

    companion object {
        private val LOGTAG = this::class.qualifiedName

        fun newInstance() =
                PollutantFragment()
    }

    private var pollutant: Pollutant? = null

    private lateinit var cardView: CardView
    private lateinit var cardViewContent: ConstraintLayout
    private lateinit var progressBarView: ProgressBar
    private lateinit var contentGroup: Group
    private lateinit var nameView: TextView
    private lateinit var shortnameView: TextView
    private lateinit var valueView: TextView
    private lateinit var unitView: TextView
    private lateinit var listener: FragmentListener


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_pollutant_tile, container, false)

        findViews(view)

        cardViewContent.setOnClickListener {
            startActivity(DetailsActivity.newIntent(context!!, pollutant?.id ?: -1))
        }

        return view
    }

    fun findViews(view: View) {
        cardView = view.findViewById(R.id.cardview)
        cardViewContent = view.findViewById(R.id.cardview_content)

        progressBarView = view.findViewById(R.id.progressBar)
        progressBarView.visibility = View.VISIBLE

        contentGroup = view.findViewById(R.id.content_group)
        contentGroup.visibility = View.GONE

        nameView = view.findViewById(R.id.name)
        shortnameView = view.findViewById(R.id.shortname)
        valueView = view.findViewById(R.id.value)
        unitView = view.findViewById(R.id.unit)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is FragmentListener) {
            listener = context
        } else {
            throw RuntimeException("activity must implement FragmentListener")
        }
    }

    fun setPollutant(aPollutant: Pollutant) {
        pollutant = aPollutant
        val valueDao = AppDatabase.get(context!!).valueDao()

        valueDao.getLatestByPollutantLive(pollutant?.id!!).observe(this, Observer<Value?> { value: Value? ->
            if (value == null) {
                Log.d(LOGTAG, "no value for pollutant with id ${pollutant?.id!!}")
                showData(pollutant!!, null)
            } else {
//                Log.d(LOGTAG, "${pollutant?.id!!} first element")
                showData(pollutant!!, value)
            }
        })
    }

    private fun showData(pollutant: Pollutant, value: Value?) {
        if (value == null) {
            progressBarView.visibility = View.VISIBLE
            contentGroup.visibility = View.GONE
            return
        }

        nameView.text = pollutant.name
        shortnameView.text = pollutant.shortname
        unitView.text = pollutant.unit
        valueView.text = "%.0f".format(value.valueDbl)

        cardViewContent.setBackgroundColor(PollutantUtils.getColorByValue(context!!, pollutant.name, value.valueDbl))

        progressBarView.visibility = View.GONE
        contentGroup.visibility = View.VISIBLE

        listener.setLastUpdatedTimestamp(value.dateAsDate())
    }

    fun hide() {
        cardView.visibility = View.GONE
        progressBarView.visibility = View.GONE
        contentGroup.visibility = View.GONE
    }

    fun show() {
        cardView.visibility = View.VISIBLE
    }

    interface FragmentListener {
        fun setLastUpdatedTimestamp(instant: Date)
    }

}

