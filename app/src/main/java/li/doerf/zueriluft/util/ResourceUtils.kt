package li.doerf.zueriluft.util

import android.content.Context

class ResourceUtils {
    companion object {
        fun getStringIdentifier(context: Context, name: String): Int {
            return context.resources.getIdentifier(name, "string", context.packageName)
        }
    }
}