package li.doerf.zueriluft.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.constraintlayout.widget.Group
import androidx.fragment.app.Fragment
import li.doerf.zueriluft.R
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.db.daos.LocationDao
import li.doerf.zueriluft.db.daos.PollutantDao
import li.doerf.zueriluft.db.entities.Location
import li.doerf.zueriluft.db.entities.Pollutant
import li.doerf.zueriluft.db.entities.Threshold
import li.doerf.zueriluft.util.PollutantLevel
import li.doerf.zueriluft.util.asString
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * A placeholder fragment containing a simple view.
 */
class ThresholdConfigurationActivityFragment : Fragment() {

    private lateinit var locationTextView: TextView
    private lateinit var ozoneLevelSpinner: Spinner
    private lateinit var ozoneGroup: Group
    private lateinit var nitrogenDioxideLevelSpinner: Spinner
    private lateinit var nitrogenDioxideGroup: Group
    private lateinit var particulatesLevelSpinner: Spinner
    private lateinit var particulatesGroup: Group

    private var locationId: Long = -1
    private var ozone: Pollutant? = null
    private var nitrogenDioxide: Pollutant? = null
    private var particulates: Pollutant? = null
    private var positionLevelMap: MutableMap<Int,PollutantLevel> = mutableMapOf()
    private var levelPositionMap: MutableMap<PollutantLevel,Int> = mutableMapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationId = arguments?.getLong(ARG_LOCATION_ID)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_threshold_configuration, container, false)

        findViews(view)

        prepareName()
        prepareLevelsMap()
        preparePollutants()

        return view
    }

    private fun findViews(view: View) {
        locationTextView = view.findViewById(R.id.location_name)
        ozoneLevelSpinner = view.findViewById(R.id.spinner_ozone_level)
        ozoneGroup = view.findViewById(R.id.g_ozone)
        nitrogenDioxideLevelSpinner = view.findViewById(R.id.spinner_no2_level)
        nitrogenDioxideGroup = view.findViewById(R.id.g_no2)
        particulatesLevelSpinner = view.findViewById(R.id.spinner_pm10_level)
        particulatesGroup = view.findViewById(R.id.g_pm10)
    }

    private fun prepareName() {
        val locationDao: LocationDao = AppDatabase.get(context!!).locationDao()

        doAsync {
            val loc = locationDao.getById(locationId)
            val data = loc!!

            uiThread {
                updateUi(data)
            }

        }
    }

    private fun updateUi(location: Location) {
        locationTextView.text = location.name
    }

    private fun prepareLevelsMap() {
        var i = 0
        PollutantLevel.values().map { l ->
            val t = i++
            positionLevelMap[t] = l
            levelPositionMap[l] = t
        }
    }

    private fun preparePollutants() {
        val pollutantDao: PollutantDao = AppDatabase.get(context!!).pollutantDao()
        doAsync {
            val pollutants =  pollutantDao.getByLocation(locationId)

            uiThread {
                processPollutants(pollutants)
                prepareSpinner(ozone, ozoneGroup, ozoneLevelSpinner)
                prepareSpinner(nitrogenDioxide, nitrogenDioxideGroup, nitrogenDioxideLevelSpinner)
                prepareSpinner(particulates, particulatesGroup, particulatesLevelSpinner)
            }
        }
    }

    private fun processPollutants(pollutants: List<Pollutant>) {
        pollutants.forEach {
            when(it.name) {
                "Ozon" -> ozone = it
                "Feinstaub" ->  particulates = it
                "Stickstoffdioxid" -> nitrogenDioxide = it
            }
        }
    }

    private fun prepareSpinner(pollutant: Pollutant?, group: Group, spinner: Spinner) {
        if (pollutant == null) {
            group.visibility = View.GONE
            return // don't configure anything further as view is hidden
        } else {
            group.visibility = View.VISIBLE
        }

        val adapter = ArrayAdapter<CharSequence>(context!!, R.layout.layout_spinner)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapter.addAll(positionLevelMap.map { e ->
            val level = e.value
            level.asString(context!!)
        })
        spinner.adapter = adapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                // cannot happen
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedLevel = positionLevelMap[position]!!
                saveThreshold(locationId, pollutant, selectedLevel)
            }

        }

        val thresholdDao = AppDatabase.get(context!!).thresholdDao()

        doAsync {
            val threshold = thresholdDao.getByLocationAndPollutant(locationId, pollutant.id!!)

            uiThread {
                setThreshold(spinner, threshold)
            }
        }
    }

    private fun setThreshold(spinner: Spinner, threshold: Threshold?) {
        if (threshold == null || ! threshold.enabled) {
            spinner.setSelection(0)
        } else {
            val level = PollutantLevel.valueOf(threshold.level)
            val i  = levelPositionMap[level]!!
            spinner.setSelection(i)
        }
    }

    private fun saveThreshold(locationId: Long, pollutant: Pollutant, level: PollutantLevel) {
        val threadholdDao = AppDatabase.get(context!!).thresholdDao()

        bg {
            val existing = threadholdDao.getByLocationAndPollutant(locationId, pollutant.id!!)
            if (existing != null) {
                if (level == PollutantLevel.NONE) {
                    existing.enabled = false
                    existing.exceeded = false
                } else {
                    existing.level = level.name
                    existing.enabled = true
                }
                threadholdDao.update(existing)
                Log.i(LOGTAG, "existing threshold updated: $existing")
            } else {
                val threshold = Threshold(null, locationId, pollutant.id!!, true, level.name, false)
                threadholdDao.insert(threshold)
                Log.i(LOGTAG, "new threshold saved: $threshold")
            }
        }
    }

    companion object {
        private val LOGTAG = this::class.qualifiedName
        private const val ARG_LOCATION_ID: String = "LocationId"

        fun create(locId: Long): ThresholdConfigurationActivityFragment {
            val args = Bundle()
            args.putLong(ARG_LOCATION_ID, locId)
            val frag = ThresholdConfigurationActivityFragment()
            frag.arguments = args
            return frag
        }
    }
}
