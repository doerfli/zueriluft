package li.doerf.zueriluft.db.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "pollutants",
        foreignKeys = [(ForeignKey(entity = Location::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("location_id"),
                onDelete = ForeignKey.CASCADE))])
data class Pollutant(@PrimaryKey(autoGenerate = true) var id: Long?,
                     @ColumnInfo(name="name") var name: String,
                     @ColumnInfo(name="shortname") var shortname: String,
                     @ColumnInfo(name="unit") var unit: String,
                     @ColumnInfo(name="location_id") var locationId: Long,
                     @ColumnInfo(name="remoteId") var remoteId: Long
) : Parcelable