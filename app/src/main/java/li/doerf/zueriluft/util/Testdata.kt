package li.doerf.zueriluft.util

import android.content.Context
import android.util.Log
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.db.entities.Value
import org.jetbrains.anko.coroutines.experimental.bg
import java.text.SimpleDateFormat
import java.util.*

class Testdata {
    companion object {
        private val LOGTAG = this::class.qualifiedName
        fun insert(context: Context) {
            Log.d(LOGTAG, "generating testdata for each pollutant in each location")
            val locationDao = AppDatabase.get(context)?.locationDao()
            val pollutantDao = AppDatabase.get(context)?.pollutantDao()
            val valueDao = AppDatabase.get(context)?.valueDao()
            val now = Calendar.getInstance()
            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:SS'Z'", Locale.GERMAN)
            sdf.timeZone = TimeZone.getTimeZone("GMT")

            bg {
                locationDao?.getAll()?.forEach { location ->
                    pollutantDao?.getByLocation(location.id!!)?.forEach{ poll ->
                        val n = Value(
                                null,
                                now.get(Calendar.MINUTE).toDouble(),
                                sdf.format(now.time),
                                poll.id!!
                        )
                        valueDao?.insert(n)
                    }
                }
            }
        }
    }
}
