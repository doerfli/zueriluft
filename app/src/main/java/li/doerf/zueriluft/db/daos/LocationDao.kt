package li.doerf.zueriluft.db.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import li.doerf.zueriluft.db.entities.Location

@Dao
interface LocationDao {
    @Insert
    fun insert(vararg entity: Location): Array<Long>

    @Update
    fun update(vararg entity: Location): Int

    @Delete
    fun delete(vararg entity: Location): Int

    @Query("SELECT * FROM locations")
    fun getAllAsLive(): LiveData<List<Location>>

    @Query("SELECT * FROM locations")
    fun getAll(): List<Location>

    @Query("SELECT * FROM locations WHERE name=:name")
    fun getByName(name: String): List<Location>

    @Query("SELECT * FROM locations WHERE remoteId=:remoteId")
    fun getByRemoteId(remoteId: Long): Location?

    @Query("SELECT * FROM locations WHERE id=:locationId")
    fun getById(locationId: Long): Location?
}