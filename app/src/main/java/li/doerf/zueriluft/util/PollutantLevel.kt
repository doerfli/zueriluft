package li.doerf.zueriluft.util

import li.doerf.zueriluft.R

enum class PollutantLevel(val colorId: Int, val order: Int) {
    NONE(R.color.value_card_default, 0),
    VERY_MINOR(R.color.value_very_minor, 1),
    MINOR(R.color.value_minor, 2),
    MODEST(R.color.value_modest, 3),
    SERIOUS(R.color.value_serious, 4),
    SEVERE(R.color.value_severe, 5),
    VERY_SEVERE(R.color.value_very_severe, 6);

    /**
     * checks if level is equal or higher that the given level (other)
     */
    fun isEqualOrHigher(other: PollutantLevel): Boolean {
        return order >= other.order
    }

}