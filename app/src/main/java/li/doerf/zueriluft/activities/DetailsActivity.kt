package li.doerf.zueriluft.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.activity_details.*
import li.doerf.zueriluft.R
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.fragments.DetailsActivityFragment
import org.jetbrains.anko.coroutines.experimental.bg

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar as Toolbar?)

        val pollutantId = intent.getLongExtra(EXTRA_POLLUTANT_ID, -1)
        val locationDao = AppDatabase.get(applicationContext).locationDao()
        val pollutantDao = AppDatabase.get(applicationContext).pollutantDao()

        bg {
            val pollutant = pollutantDao.getById(pollutantId)
            val location = locationDao.getById(pollutant?.locationId!!)
            title = "${location?.name} / ${pollutant?.name}"
        }

            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment, DetailsActivityFragment.createFragment(pollutantId))
                .commit()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    companion object {
        private const val EXTRA_POLLUTANT_ID: String = "PollutantId"

        fun newIntent(context: Context, pollutantId: Long): Intent {
            val i = Intent(context, DetailsActivity::class.java)
            i.putExtra(EXTRA_POLLUTANT_ID, pollutantId)
            return i
        }
    }

}
