package li.doerf.zueriluft.util

import android.content.Context

fun PollutantLevel.asString(context: Context): String {
    val r = "value_${name.toLowerCase()}"
    val i = ResourceUtils.getStringIdentifier(context, r)
    return context.getString(i)
}
