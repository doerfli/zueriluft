package li.doerf.zueriluft.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import li.doerf.zueriluft.R
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.db.daos.LocationDao
import li.doerf.zueriluft.db.daos.PollutantDao
import li.doerf.zueriluft.db.daos.ValueDao
import li.doerf.zueriluft.db.entities.Location
import li.doerf.zueriluft.db.entities.Pollutant
import li.doerf.zueriluft.db.entities.Value
import li.doerf.zueriluft.util.dateAsDate
import org.jetbrains.anko.coroutines.experimental.bg
import java.text.SimpleDateFormat
import java.util.*


/**
 * A placeholder fragment containing a simple view.
 */
class DetailsActivityFragment : Fragment(), OnMapReadyCallback {
    private lateinit var locationDao: LocationDao
    private lateinit var pollutantDao: PollutantDao
    private lateinit var valueDao: ValueDao
    private lateinit var chart: BarChart
    private lateinit var map: SupportMapFragment
    private lateinit var values: List<Value>
    private lateinit var xAxis: XAxis

    private var pollutantId: Long = -1
    private var pollutant: Pollutant? = null
    private var location: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationDao = AppDatabase.get(context!!).locationDao()
        pollutantDao = AppDatabase.get(context!!).pollutantDao()
        valueDao =  AppDatabase.get(context!!).valueDao()
        pollutantId = arguments?.getLong(ARGUMENT_POLLUTANT_ID) ?: -1
        Log.d(LOGTAG, "onCreate pollutantId $pollutantId")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_details, container, false)

        findViews(view)
        initializeMaps(view)

        return view
    }

    private fun initializeMaps(view: View?) {
        map.getMapAsync(this)
    }

    override fun onStart() {
        super.onStart()
        configureChart()
        retrievePollutantData()
    }

    private fun configureChart() {
        val desc = Description()
        desc.text = ""
        chart.description = desc


        val legend = chart.legend
        legend.isEnabled = false

        xAxis = chart.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
    }

    private fun retrievePollutantData() {
        bg {
            pollutant = pollutantDao.getById(pollutantId)
            location = locationDao.getById(pollutant?.locationId!!)
            val sinceDate = Calendar.getInstance()
            sinceDate.timeZone = TimeZone.getTimeZone("UTC")
            sinceDate.add(Calendar.DAY_OF_YEAR, -1)
            val since = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.GERMAN).format(sinceDate.time)
            Log.d(LOGTAG, "retrieving all values since $since")
            values = valueDao.getAllByPollutantSince(pollutant?.id!!, since)
            Log.d(LOGTAG, "found ${values.size} values")

            showChart()
        }
    }

    private fun showChart() {
        val entries = ArrayList<BarEntry>()
        values.forEachIndexed { i, v ->
            val entry = BarEntry(i.toFloat(), v.valueDbl.toFloat())
            entries.add(entry)
        }

        val set = BarDataSet(entries, pollutant?.name)
        set.color = context?.resources?.getColor(R.color.colorPrimaryDark)!!
        set.setDrawValues(false)

        val data = BarData(set)
        chart.data = data
        chart.setFitBars(true) // make the x-axis fit exactly all bars

        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz", Locale.GERMAN)
        val sdfHour = SimpleDateFormat("HH", Locale.GERMAN)

        val xAxisStrings = values.map {
            val date =  it.dateAsDate()
            context?.getString(R.string.xaxis_hour, sdfHour.format(date))!!
        }.toTypedArray()
        xAxis.valueFormatter = XAxisFormatter(xAxisStrings)

        chart.invalidate() // refresh
    }

    private fun findViews(view: View) {
        chart = view.findViewById(R.id.chart)
        map = (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if (location == null ) {
            Log.w(LOGTAG, "location was null")
            return
        }
        val middle = LatLng(location?.latitude!!, location?.longitude!!)
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(middle, 12.5f))
        googleMap?.addMarker(
                MarkerOptions()
                        .position(middle)
                        .title(location?.name)
        )
    }

    companion object {
        private const val ARGUMENT_POLLUTANT_ID: String = "PollutantId"
        private val LOGTAG = this::class.qualifiedName

        fun createFragment(pollutantId: Long): Fragment {
            val f = DetailsActivityFragment()
            val b = Bundle()
            b.putLong(ARGUMENT_POLLUTANT_ID, pollutantId)
            f.arguments = b
            return f
        }
    }

    class XAxisFormatter(val values: Array<String>) : ValueFormatter() {

        override fun getFormattedValue(value: Float, axis: AxisBase?): String {
            return values[value.toInt()]
        }

    }
}
