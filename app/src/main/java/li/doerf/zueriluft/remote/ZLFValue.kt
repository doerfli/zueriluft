package li.doerf.zueriluft.remote

data class ZLFValue(var value: Double, var instant: String, val pollutantId: Long?)
