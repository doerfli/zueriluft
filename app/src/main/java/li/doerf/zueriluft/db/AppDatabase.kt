package li.doerf.zueriluft.db

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import li.doerf.zueriluft.db.daos.LocationDao
import li.doerf.zueriluft.db.daos.PollutantDao
import li.doerf.zueriluft.db.daos.ThresholdDao
import li.doerf.zueriluft.db.daos.ValueDao
import li.doerf.zueriluft.db.entities.Location
import li.doerf.zueriluft.db.entities.Pollutant
import li.doerf.zueriluft.db.entities.Threshold
import li.doerf.zueriluft.db.entities.Value


@Database(entities = [Location::class, Pollutant::class, Value::class, Threshold::class], version = 5)
abstract class AppDatabase : RoomDatabase() {

    abstract fun locationDao(): LocationDao
    abstract fun pollutantDao(): PollutantDao
    abstract fun valueDao(): ValueDao
    abstract fun thresholdDao(): ThresholdDao

    companion object {
        private var INSTANCE: AppDatabase? = null
        private val LOGTAG = this::class.qualifiedName

        fun get(context: Context): AppDatabase {
            synchronized(AppDatabase::class) {
                if (INSTANCE == null) {
                    INSTANCE = createInstance(context)
                }
            }
            return INSTANCE!!
        }

        private fun createInstance(context: Context) = Room.databaseBuilder(context,
                AppDatabase::class.java, "zueriluft.db")
                .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5)
                .build()

        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE `thresholds` " +
                        "(" +
                        "`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`location_id` INTEGER NOT NULL, " +
                        "`pollutant_id` INTEGER NOT NULL, " +
                        "`enabled` INTEGER NOT NULL, " +
                        "`level` TEXT NOT NULL, " +
                        "`exceeded` INTEGER NOT NULL, " +
                        "FOREIGN KEY(`location_id`) REFERENCES `locations`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE , " +
                        "FOREIGN KEY(`pollutant_id`) REFERENCES `pollutants`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE " +
                        ")")
            }
        }

        private val MIGRATION_2_3: Migration = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE `locations` " +
                        "ADD `latitude` REAL")
                database.execSQL("ALTER TABLE `locations` " +
                        "ADD `longitude` REAL")
            }
        }

        private val MIGRATION_3_4: Migration = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                Log.i(LOGTAG, "dumping all database content (locations, pollutants, values) to remove duplicate data")
                database.execSQL("DELETE FROM `values`")
                database.execSQL("DELETE FROM `pollutants`")
                database.execSQL("DELETE FROM `locations`")
            }
        }

        private val MIGRATION_4_5: Migration = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                Log.i(LOGTAG, "dumping all thresholds")
                database.execSQL("DELETE FROM `thresholds`")
            }
        }

    }

}