package li.doerf.zueriluft.util

import li.doerf.zueriluft.ISO_DATETIME_PATTERN
import li.doerf.zueriluft.db.entities.Value
import java.text.SimpleDateFormat
import java.util.*

fun Value.dateAsDate(): Date {
    val sdf = SimpleDateFormat(ISO_DATETIME_PATTERN, Locale.GERMAN)
    sdf.timeZone = TimeZone.getTimeZone("UTC") // data provided is UTC+1
    return sdf.parse(dateStr)
}