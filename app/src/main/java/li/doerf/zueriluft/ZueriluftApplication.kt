package li.doerf.zueriluft

import android.app.Activity
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.multidex.MultiDexApplication
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import li.doerf.zueriluft.services.DownloadDataWorker
import java.util.concurrent.TimeUnit

class ZueriluftApplication : MultiDexApplication(), Application.ActivityLifecycleCallbacks {

    companion object {
        private val LOGTAG = this::class.qualifiedName
        private val JOB_TAG = "zueriluft-background-download-job"
    }

    private var activityReferences = 0
    private var isActivityChangingConfigurations = false
    private var isAppInForeground = false

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(this)
        registerNotificationChannel()
        scheduleDataDownload()
    }

    private fun scheduleDataDownload() {
        Log.d(LOGTAG, "scheduling synchronization")
        val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()
        val checkWorker = PeriodicWorkRequest.Builder(DownloadDataWorker::class.java,
                1, TimeUnit.HOURS)
                .addTag(JOB_TAG)
                .setConstraints(constraints)
        WorkManager.getInstance().enqueue(checkWorker.build())
        Log.i(LOGTAG, "scheduled job")
    }

    private fun registerNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val description = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun isAppInForeground(): Boolean {
        return isAppInForeground
    }

    override fun onActivityResumed(activity: Activity?) {
    }

    override fun onActivityStarted(activity: Activity?) {
        if (++activityReferences == 1 && !isActivityChangingConfigurations) {
            isAppInForeground = true
            Log.d(LOGTAG, "isAppInForeground: $isAppInForeground")
        }
    }

    override fun onActivityDestroyed(activity: Activity?) {
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
    }

    override fun onActivityStopped(activity: Activity?) {
        isActivityChangingConfigurations = activity!!.isChangingConfigurations
        if (--activityReferences == 0 && !isActivityChangingConfigurations) {
            isAppInForeground = false
            Log.d(LOGTAG, "isAppInForeground: $isAppInForeground")
        }
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
    }

    override fun onActivityPaused(activity: Activity?) {
    }

}