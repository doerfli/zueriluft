package li.doerf.zueriluft.db.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "values",
        foreignKeys = [(ForeignKey(entity = Pollutant::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("pollutant_id"),
                onDelete = ForeignKey.CASCADE))])
data class Value(@PrimaryKey(autoGenerate = true) var id: Long?,
                 @ColumnInfo(name="value") var valueDbl: Double,
                 @ColumnInfo(name="date") var dateStr: String,
                 @ColumnInfo(name="pollutant_id") var pollutantId: Long
) : Parcelable
