package li.doerf.zueriluft.db.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import li.doerf.zueriluft.db.entities.Pollutant

@Dao
interface PollutantDao {
    @Insert
    fun insert(vararg entity: Pollutant): Array<Long>

    @Update
    fun update(vararg entity: Pollutant): Int

    @Delete
    fun delete(vararg entity: Pollutant): Int

    @Query("SELECT * FROM pollutants")
    fun getAll(): List<Pollutant>

    @Query("SELECT * FROM pollutants")
    fun getAllAsLive(): LiveData<List<Pollutant>>

    @Query("SELECT * FROM pollutants WHERE name=:name AND location_id=:locationId")
    fun getByNameAndLocation(name: String, locationId: Long): List<Pollutant>

    @Query("SELECT * FROM pollutants WHERE location_id=:locationId")
    fun getByLocation(locationId: Long): List<Pollutant>

    @Query("SELECT * FROM pollutants WHERE id=:pollutantId")
    fun getById(pollutantId: Long): Pollutant?

    @Query("SELECT * FROM pollutants WHERE remoteId=:remotePollutantId")
    fun getByRemoteId(remotePollutantId: Long): List<Pollutant>
}