package li.doerf.zueriluft.remote

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ZueriLuftFlowService {
    @GET("locations")
    fun getLocations(): Call<List<ZLFLocation>>

    @GET("pollutants/{locationId}")
    fun getPollutants(@Path("locationId") locationId: Long): Call<List<ZLFPollutant>>

    @GET("values/{locationId}")
    fun getLatestValues(@Path("locationId") locationId: Long): Call<List<ZLFValue>>

    @GET("values/{locationId}/{date}")
    fun getAllValuesSince(@Path("locationId") locationId: Long, @Path("date") date: String): Call<List<ZLFValue>>
}