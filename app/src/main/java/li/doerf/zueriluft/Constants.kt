package li.doerf.zueriluft

const val BASE_URL: String = "https://zueriluft.bytes.li/"
const val BROADCAST_ACTION_FINISH_DOWNLOAD = "li.doerf.zueriluft.BROADCAST_FINISH_DOWNLOAD"
val POLLUTANTS_TO_SHOW: Array<String> = arrayOf("Ozon", "Feinstaub", "Stickstoffdioxid", "Lufttemperatur")
val POLLUTANTS_TO_ACTIVATE_NOTIFICATION: Array<String> = arrayOf("Ozon", "Feinstaub", "Stickstoffdioxid")
const val CHANNEL_ID: String = "ZueriluftNotifications"
const val ISO_DATETIME_PATTERN: String = "yyyy-MM-dd'T'HH:mm:ss'Z'"
