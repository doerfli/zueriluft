package li.doerf.zueriluft.activities

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import li.doerf.zueriluft.POLLUTANTS_TO_SHOW
import li.doerf.zueriluft.R
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.widget.PollutantWidget
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class WidgetConfigurationActivity : AppCompatActivity() {
    private var appWidgetId: Int = AppWidgetManager.INVALID_APPWIDGET_ID
    private var locationId: Long = -1
    private var pollutantId: Long = -1
    private var cancelled: Boolean = false

    private lateinit var locationName: Spinner
    private lateinit var pollutantName: Spinner
    private lateinit var locationAdapter: ArrayAdapter<CharSequence>
    private lateinit var pollutantAdapter: ArrayAdapter<CharSequence>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_widget_configuration)

        val intent = intent
        val extras = intent.extras
        if (extras != null) {
            appWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID)
        }

        title = getString(R.string.configure_widget)

        prepareAdapters()
        prepareLocationSpinner()
        preparePollutantSpinner()
        prepareButtons()

        doAsync {
            val t = AppDatabase.get(applicationContext)?.locationDao()?.getAll()!!.map { it.name }

            uiThread {
                showLocationData(t)
            }

        }
    }

    fun prepareAdapters() {
        locationAdapter = ArrayAdapter(this, R.layout.layout_spinner)
        locationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        pollutantAdapter = ArrayAdapter(this, R.layout.layout_spinner)
        pollutantAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    }

    fun prepareLocationSpinner() {
        locationName = findViewById(R.id.location_name)
        locationName.adapter = locationAdapter
        locationName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Log.d(LOGTAG, "location item selected: $position")
                doAsync {
                    val locationDao = AppDatabase.get(applicationContext)?.locationDao()!!
                    val newLocaId = locationDao.getByName(locationName.selectedItem.toString()).first().id!!
                    processNewLocationId(newLocaId)
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }
    }

    fun preparePollutantSpinner() {
        pollutantName = findViewById(R.id.pollutant_name)
        pollutantName.adapter = pollutantAdapter
        pollutantName.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Log.d(LOGTAG, "pollutant item selected: $position")
                bg {
                    val pollutantDao = AppDatabase.get(applicationContext)?.pollutantDao()!!
                    val newPollId = pollutantDao.getByNameAndLocation(pollutantName.selectedItem.toString(), locationId).first().id!!
                    if (newPollId != pollutantId) {
                        Log.d(LOGTAG, "updating pollutant id")
                        pollutantId = newPollId
                    }
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }
    }

    private fun prepareButtons() {
        val finishButton = findViewById<Button>(R.id.finish)
        finishButton.setOnClickListener {
            saveResults()
        }

        val cancelButton = findViewById<Button>(R.id.cancel)
        cancelButton.setOnClickListener {
            cancelled = true
            configurationFinished()
        }
    }

    override fun onResume() {
        super.onResume()
        cancelled = false
    }

    private fun processNewLocationId(newLocationId: Long) {
        if (newLocationId!=locationId) {
            Log.d(LOGTAG, "updating location id")
            locationId = newLocationId

            doAsync {
                val pollutants = AppDatabase.get(applicationContext)?.pollutantDao()?.getByLocation(newLocationId)!!.map { it.name }.filter {
                        POLLUTANTS_TO_SHOW.contains(it)

                }

                uiThread {
                    showPollutantData(pollutants)
                }

            }
        }
    }

    fun showLocationData(data: List<String>) {
        locationAdapter.clear()
        locationAdapter.addAll(data)
        locationName.adapter = locationAdapter
    }

    fun showPollutantData(data: List<String>) {
        pollutantAdapter.clear()
        pollutantAdapter.addAll(data)
        pollutantName.adapter = pollutantAdapter
    }

    private fun saveResults() {
        val settings = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val editor = settings.edit()
        editor.putLong("Widget_${appWidgetId}_pollutantId", pollutantId)
        editor.apply()
        configurationFinished()
    }

    fun configurationFinished() {
        val resultValue = Intent()
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)

        if (!cancelled) {
            val appWidgetManager = AppWidgetManager.getInstance(applicationContext)
            PollutantWidget.updateAppWidget(applicationContext, appWidgetManager, appWidgetId)
            setResult(Activity.RESULT_OK, resultValue)
        } else {
            setResult(Activity.RESULT_CANCELED, resultValue)
        }

        finish()
    }

    companion object {
        private val LOGTAG = this::class.qualifiedName
    }
}
