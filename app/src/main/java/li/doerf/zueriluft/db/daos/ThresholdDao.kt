package li.doerf.zueriluft.db.daos

import androidx.room.*
import li.doerf.zueriluft.db.entities.Threshold

@Dao
interface ThresholdDao {
    @Insert
    fun insert(vararg entity: Threshold): Array<Long>

    @Update
    fun update(vararg entity: Threshold): Int

    @Delete
    fun delete(vararg entity: Threshold): Int

    @Query("SELECT * FROM thresholds")
    fun getAll(): List<Threshold>

    @Query("SELECT * FROM thresholds where location_id=:locationId AND pollutant_id=:pollutantId")
    fun getByLocationAndPollutant(locationId: Long, pollutantId: Long): Threshold?

    @Query("SELECT COUNT(*) FROM thresholds where location_id=:locationId and enabled=1")
    fun getCountByLocation(locationId: Long): Int

    @Query("SELECT * FROM thresholds where enabled=1 and exceeded=0")
    fun getEnabledNotExceeded(): List<Threshold>

    @Query("SELECT * FROM thresholds where exceeded=1")
    fun getExceeded(): List<Threshold>
}
