package li.doerf.zueriluft.db.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "thresholds",
        foreignKeys = [
            ForeignKey(entity = Location::class,
                    parentColumns = arrayOf("id"),
                    childColumns = arrayOf("location_id"),
                    onDelete = ForeignKey.CASCADE),
            ForeignKey(entity = Pollutant::class,
                    parentColumns = arrayOf("id"),
                    childColumns = arrayOf("pollutant_id"),
                    onDelete = ForeignKey.CASCADE)
        ])
data class Threshold(@PrimaryKey(autoGenerate = true) var id: Long?,
                     @ColumnInfo(name="location_id") var locationId: Long,
                     @ColumnInfo(name="pollutant_id") var pollutantId: Long,
                     @ColumnInfo(name="enabled") var enabled: Boolean,
                     @ColumnInfo(name="level") var level: String,
                     @ColumnInfo(name="exceeded") var exceeded: Boolean
) : Parcelable