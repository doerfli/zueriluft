package li.doerf.zueriluft.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import li.doerf.zueriluft.R

class HelpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)

        title = getString(R.string.title_colors_explain)
    }
}
