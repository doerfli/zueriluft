package li.doerf.zueriluft.remote

data class ZLFPollutant(var id: Long, var name: String, var shortName: String, var unit: String)