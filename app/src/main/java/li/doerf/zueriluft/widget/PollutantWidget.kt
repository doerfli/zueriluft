package li.doerf.zueriluft.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import android.widget.RemoteViews
import li.doerf.zueriluft.R
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.db.entities.Location
import li.doerf.zueriluft.db.entities.Pollutant
import li.doerf.zueriluft.db.entities.Value
import li.doerf.zueriluft.util.PollutantUtils
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * Implementation of App Widget functionality.
 */
class PollutantWidget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    companion object {
        private val LOGTAG = this::class.qualifiedName

        fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager,
                                     appWidgetId: Int) {
            val settings = PreferenceManager.getDefaultSharedPreferences(context)
            val pollutantId = settings.getLong("Widget_${appWidgetId}_pollutantId", -1)

            val locationDao = AppDatabase.get(context)?.locationDao()!!
            val pollutantDao = AppDatabase.get(context)?.pollutantDao()!!
            val valueDao = AppDatabase.get(context)?.valueDao()!!

            doAsync {
                Log.d(LOGTAG, "retrieving data for pollutant with id $pollutantId")
                val pollutant = pollutantDao.getById(pollutantId)
                if (pollutant == null ) {
                    Log.e(LOGTAG, "unknown pollutant with id $pollutantId")
                }
                val location = locationDao.getById(pollutant!!.locationId)
                if (location == null ) {
                    Log.e(LOGTAG, "unknown location with id ${pollutant.locationId}")
                }
                val value = valueDao.getLatestByPollutant(pollutantId)
                val data = Triple(location, pollutant, value)

                uiThread {
                    showData(context, appWidgetManager, appWidgetId, data)
                }
            }



        }

        private fun showData(context: Context, appWidgetManager: AppWidgetManager,
                     appWidgetId: Int, data: Triple<Location?, Pollutant?, Value?>) {
            val (location, pollutant, value) = data
            Log.d(LOGTAG, "building widget view for $value")

            val views = RemoteViews(context.packageName, R.layout.pollutant_widget)
//
            if (location == null || pollutant == null || value == null ) {
                Log.e(LOGTAG, "could not build widget. location $location, pollutant $pollutant, value $value")
                appWidgetManager.updateAppWidget(appWidgetId, views)
                return
            }

            Log.d(LOGTAG, "location $location, pollutant $pollutant, value $value")
            // Construct the RemoteViews object
            views.setTextViewText(R.id.location_name, location.name)
            views.setTextViewText(R.id.shortname, pollutant.shortname)
            views.setTextViewText(R.id.value, "%.0f".format(value.valueDbl))
            views.setTextViewText(R.id.unit, pollutant.unit)

            val color = PollutantUtils.getColorByValue(context, pollutant.name, value.valueDbl)
            views.setInt(R.id.widgetlayout, "setBackgroundColor", color)

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}

