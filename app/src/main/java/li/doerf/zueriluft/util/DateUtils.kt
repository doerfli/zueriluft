package li.doerf.zueriluft.util

import li.doerf.zueriluft.ISO_DATETIME_PATTERN
import java.text.SimpleDateFormat
import java.util.*

fun Date.asIsoString(): String {
    val sdf = SimpleDateFormat(ISO_DATETIME_PATTERN, Locale.GERMAN)
    return sdf.format(this)
}