package li.doerf.zueriluft.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import li.doerf.zueriluft.BROADCAST_ACTION_FINISH_DOWNLOAD
import li.doerf.zueriluft.POLLUTANTS_TO_SHOW
import li.doerf.zueriluft.R
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.db.entities.Location
import li.doerf.zueriluft.db.entities.Pollutant
import li.doerf.zueriluft.fragments.PollutantFragment
import li.doerf.zueriluft.services.DownloadDataWorker
import li.doerf.zueriluft.util.DeviceUtils
import li.doerf.zueriluft.util.Testdata
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity(), PollutantFragment.FragmentListener {
    private lateinit var locationName: Spinner
    private lateinit var thresholdsImageButton: ImageButton
    private lateinit var updatedAtTextView: TextView
    private var locationId: Long = 0
    private var fragmentList: MutableList<PollutantFragment> = mutableListOf()
    private var locationNameMapIdPosition: MutableMap<Long,Int> = mutableMapOf()
    private var locationNameMapPositionId: MutableMap<Int,Long> = mutableMapOf()
    private val responseReceiver: SyncFinishedReceiver = SyncFinishedReceiver()

    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createFragments()

        findViews()
        prepareLocations()
        prepareButtons()
        registerReceiver()
    }

    override fun onDestroy() {
        unregisterReceiver()
        super.onDestroy()
    }

    private fun createFragments() {
        fragmentList.add(PollutantFragment.newInstance())
        fragmentList.add(PollutantFragment.newInstance())
        fragmentList.add(PollutantFragment.newInstance())
        fragmentList.add(PollutantFragment.newInstance())

        supportFragmentManager
                .beginTransaction()
                .add(R.id.card_1, fragmentList[0])
                .add(R.id.card_2, fragmentList[1])
                .add(R.id.card_3, fragmentList[2])
                .add(R.id.card_4, fragmentList[3])
                .commit()
    }

    private fun findViews() {
        updatedAtTextView = findViewById(R.id.updated_at)
        thresholdsImageButton = findViewById(R.id.thresholds)
        locationName = findViewById(R.id.location_name)
    }

    fun prepareLocations() {
        val adapter = ArrayAdapter<CharSequence>(this, R.layout.layout_spinner)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        locationName.adapter = adapter
        locationName.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Log.d(LOGTAG, "item selected: $position")
                bg {
                    val newLocaId = locationNameMapPositionId[position]!!
                    if (newLocaId != locationId) {
                        setNewLocation(newLocaId)
                    }

                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }
        // register location observer
        AppDatabase.get(applicationContext)?.locationDao()?.getAllAsLive()?.observe(this, androidx.lifecycle.Observer { locationList: List<Location>? ->
            adapter.clear()
            var i = 0
            locationNameMapIdPosition.clear()
            locationNameMapPositionId.clear()
            adapter.addAll(locationList!!.map {
                locationNameMapIdPosition[it.id!!] = i
                locationNameMapPositionId[i++] = it.id!!
                it.name
            })
            setSelectedPage()
        })
    }

    private fun setSelectedPage() {
        val locationId = intent.getLongExtra(EXTRA_LOCATION_ID, -1)

        if(locationId == -1L) {
            locationName.setSelection(0)
            return
        }

        locationName.setSelection(locationNameMapIdPosition[locationId]!!)
    }

    private fun prepareButtons() {
        thresholdsImageButton.setOnClickListener {
            val intent = Intent(applicationContext, ThresholdConfigurationActivity::class.java)
            intent.putExtra(ThresholdConfigurationActivity.EXTRA_LOCATION_ID, locationId)
            startActivity(intent)
        }

        val helpButton = findViewById<Button>(R.id.help)
        helpButton.setOnClickListener {
            startActivity(Intent(applicationContext, HelpActivity::class.java))
        }
    }

    private fun registerReceiver() {
        val intentFilter = IntentFilter(BROADCAST_ACTION_FINISH_DOWNLOAD)
        responseReceiver.activity = this
        LocalBroadcastManager.getInstance(this).registerReceiver(responseReceiver, intentFilter)
    }

    private fun unregisterReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(responseReceiver)
    }

    fun setNewLocation(newLocaId: Long) {
        Log.d(LOGTAG, "updating location id to $newLocaId")
        locationId = newLocaId
        setNotificationsImage()
        getLatestPollutantsAndUpdateFragments()
    }

    fun setNotificationsImage() {
        val thresholdsDao = AppDatabase.get(applicationContext).thresholdDao()
        if (thresholdsDao.getCountByLocation(locationId) > 0) {
            thresholdsImageButton.setImageResource(R.drawable.ic_notifications_on)
        } else {
            thresholdsImageButton.setImageResource(R.drawable.ic_notifications_off)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        if(DeviceUtils.isEmulator()) {
            val testdataMenu = menu.findItem(R.id.action_addtestdataentry)
            testdataMenu.isVisible = true
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        when(id) {
            R.id.action_addtestdataentry -> {
                Testdata.insert(applicationContext)
                return true
            }
            R.id.action_sync -> {
                DownloadDataWorker.runNow()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun getLatestPollutantsAndUpdateFragments() {
        Log.d(LOGTAG, "update pollutants for location $locationId into fragments")
        val pollutantDao = AppDatabase.get(applicationContext)?.pollutantDao()!!

        doAsync {
            val data = pollutantDao.getByLocation(locationId)

            uiThread {
                // This code is executed on the UI thread
                updatePollutantsInFragments(data)
            }

        }
    }

    private fun updatePollutantsInFragments(pollutants: List<Pollutant>) {
        var i = 0
        for (pollutant in pollutants) {
            if (!POLLUTANTS_TO_SHOW.contains(pollutant.name)) {
                continue
            }
            fragmentList[i].setPollutant(pollutant)
            fragmentList[i++].show()
        }
        while (i < 4) {
            fragmentList[i++].hide()
        }
    }

    override fun setLastUpdatedTimestamp(date: Date) {
        val sdf = SimpleDateFormat("dd.MM.yy HH:mm 'Uhr'", Locale.GERMAN)
        // should be GMT+1, but that time denotes the start of the measurement period, whereas we
        // want the end of the measuremnt period here. To simplify calculation we just use GMT+2 instead
        sdf.timeZone = TimeZone.getTimeZone("GMT+2")
        updatedAtTextView.text = getString(R.string.last_update, sdf.format(date))
    }

    class SyncFinishedReceiver : BroadcastReceiver() {
        lateinit var activity: MainActivity

        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d(LOGTAG, "SyncFinishedReceiver.onReceive")
            activity.getLatestPollutantsAndUpdateFragments()
        }

    }

    companion object {
        const val EXTRA_LOCATION_ID: String = "LocationId"
        private val LOGTAG = this::class.qualifiedName
    }
}


