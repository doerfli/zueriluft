package li.doerf.zueriluft.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import li.doerf.zueriluft.R
import li.doerf.zueriluft.fragments.ThresholdConfigurationActivityFragment

class ThresholdConfigurationActivity : AppCompatActivity() {

    companion object {
        val EXTRA_LOCATION_ID: String = "LocationId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_threshold_configuration)
        actionBar?.setDisplayHomeAsUpEnabled(true)

        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment, ThresholdConfigurationActivityFragment.create(intent.getLongExtra(EXTRA_LOCATION_ID, -1)))
                .commit()

        title = getString(R.string.notifications)
    }

}
