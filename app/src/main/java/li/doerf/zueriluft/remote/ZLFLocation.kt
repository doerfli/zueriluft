package li.doerf.zueriluft.remote

data class ZLFLocation(var id: Long, var name: String, var latitude: Double, var longitude: Double)