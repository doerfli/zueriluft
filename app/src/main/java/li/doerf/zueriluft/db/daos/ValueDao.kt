package li.doerf.zueriluft.db.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import li.doerf.zueriluft.db.entities.Value

@Dao
interface ValueDao {
    @Insert
    fun insert(vararg entity: Value): Array<Long>

    @Update
    fun update(vararg entity: Value): Int

    @Delete
    fun delete(vararg entity: Value): Int

    @Query("SELECT * FROM `values`")
    fun getAll(): List<Value>

    @Query("SELECT * FROM `values` where pollutant_id=:pollutantId AND date=:dateStr ORDER BY id DESC LIMIT 1")
    fun getByPollutantAndDate(pollutantId: Long, dateStr: String): Value?

    @Query("SELECT * FROM `values` where pollutant_id=:pollutantId AND date>=:dateStr ORDER BY date")
    fun getAllByPollutantSince(pollutantId: Long, dateStr: String): List<Value>


    @Query("SELECT * FROM `values` where pollutant_id=:pollutantId ORDER BY date DESC LIMIT 1")
    fun getLatestByPollutant(pollutantId: Long): Value?

    @Query("SELECT * FROM `values` where pollutant_id=:pollutantId ORDER BY date DESC LIMIT 1")
    fun getLatestByPollutantLive(pollutantId: Long): LiveData<Value?>
}