package li.doerf.zueriluft.util

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.TaskStackBuilder
import li.doerf.zueriluft.CHANNEL_ID
import li.doerf.zueriluft.R
import li.doerf.zueriluft.activities.MainActivity
import li.doerf.zueriluft.services.CheckThresholdsWorker
import java.util.concurrent.atomic.AtomicInteger


class NotificationUtils {

    companion object {
        private const val GROUP_KEY_THRESHOLD_EXCEEDED = "li.doerf.zuerilift.ThresholdExceeded"
        private const val THRESHOLDEXCEEDEDID = 0
        private val notificationId: AtomicInteger = AtomicInteger()

        fun issueThresholdExceededNotification(context: Context, threshold: CheckThresholdsWorker.ThresholdExceeded, groupNotification: Boolean) {
            val levelStr = threshold.latestLevel.asString(context)
            val title = context.getString(R.string.notification_threshold_exceeded_title, threshold.pollutant.name)
            val text = context.getString(R.string.notification_threshold_exceeded_text, levelStr, threshold.location.name)

            val startActivityIntent = createPendingIntent(context, threshold)

            var mBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_threshold_exceeded)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(startActivityIntent)
                    .setAutoCancel(true)

            if (groupNotification) {
                mBuilder.setGroup(GROUP_KEY_THRESHOLD_EXCEEDED)
            }

            val notificationManager = NotificationManagerCompat.from(context)
            notificationManager.notify(notificationId.getAndIncrement(), mBuilder.build())
        }

        fun createPendingIntent(context: Context, threshold: CheckThresholdsWorker.ThresholdExceeded?): PendingIntent? {
            val resultIntent = Intent(context, MainActivity::class.java)
            if(threshold != null) {
                resultIntent.putExtra(MainActivity.EXTRA_LOCATION_ID, threshold.location.id!!)
            }
            // Create the TaskStackBuilder and add the intent, which inflates the back stack
            val stackBuilder = TaskStackBuilder.create(context)
            stackBuilder.addNextIntentWithParentStack(resultIntent)
            // Get the PendingIntent containing the entire back stack
            return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        fun issueThresholdExceededGroupNotification(context: Context, thresholdsExceed: List<CheckThresholdsWorker.ThresholdExceeded>) {
            val groupTitle = context.getString(R.string.notification_group_threshold_exceeded_title)
            val groupSummaryText = thresholdsExceed.map { it.location.name }.distinct().joinToString(" ")
            var s = NotificationCompat.InboxStyle()
                    .setBigContentTitle(groupTitle)
                    .setSummaryText(groupSummaryText)
            thresholdsExceed.forEach {
                val levelStr = it.latestLevel.asString(context)
                s = s.addLine("${it.location.name} ${it.pollutant.name} $levelStr")
            }
            val startActivityIntent = createPendingIntent(context, null)
            val summaryNotificationBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
                    .setContentTitle(groupTitle)
                    //set content text to support devices running API level < 24
                    .setContentText(groupSummaryText)
                    .setSmallIcon(R.drawable.ic_threshold_exceeded)
                    //build summary info into InboxStyle template
                    .setStyle(s)
                    //specify which group this notification belongs to
                    .setGroup(GROUP_KEY_THRESHOLD_EXCEEDED)
                    //set this notification as the summary for the group
                    .setGroupSummary(true)
                    .setAutoCancel(true)
                    .setContentIntent(startActivityIntent)

            val notificationManager = NotificationManagerCompat.from(context)
            notificationManager.notify(THRESHOLDEXCEEDEDID, summaryNotificationBuilder.build())

        }
    }
}