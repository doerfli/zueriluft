package li.doerf.zueriluft.util

import android.content.Context

class PollutantUtils {
    companion object {
        fun getColorByValue(context: Context, pollutantName: String, valueDbl: Double): Int {
            val level = getLevelByPollutant(pollutantName, valueDbl)
            return context?.resources?.getColor(level.colorId)!!
        }

        fun getLevelByPollutant(pollutantName: String, valueDbl: Double): PollutantLevel {
            when(pollutantName) {
                "Ozon" -> {
                    when(valueDbl) {
                        in 0.0 .. 50.0 -> return PollutantLevel.VERY_MINOR
                        in 50.0 .. 100.0 -> return PollutantLevel.MINOR
                        in 100.0 .. 120.0 -> return PollutantLevel.MODEST
                        in 120.0 .. 180.0 -> return PollutantLevel.SERIOUS
                        in 180.0 .. 240.0 -> return PollutantLevel.SEVERE
                        else -> return PollutantLevel.VERY_SEVERE
                    }
                }
                "Stickstoffdioxid" -> {
                    when(valueDbl) {
                        in 0.0 .. 10.0 -> return PollutantLevel.VERY_MINOR
                        in 10.0 .. 30.0 -> return PollutantLevel.MINOR
                        in 30.0 .. 80.0 -> return PollutantLevel.MODEST
                        in 80.0 .. 120.0 -> return PollutantLevel.SERIOUS
                        in 120.0 .. 160.0 -> return PollutantLevel.SEVERE
                        else -> return PollutantLevel.VERY_SEVERE
                    }
                }
                "Feinstaub" -> {
                    when(valueDbl) {
                        in 0.0 .. 10.0 -> return PollutantLevel.VERY_MINOR
                        in 10.0 .. 20.0 -> return PollutantLevel.MINOR
                        in 20.0 .. 50.0 -> return PollutantLevel.MODEST
                        in 50.0 .. 75.0 -> return PollutantLevel.SERIOUS
                        in 75.0 .. 100.0 -> return PollutantLevel.SEVERE
                        else -> return PollutantLevel.VERY_SEVERE
                    }
                }
                else -> {
                    return PollutantLevel.NONE
                }
            }
        }
    }
}