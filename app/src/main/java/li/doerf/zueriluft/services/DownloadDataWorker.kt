package li.doerf.zueriluft.services

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import li.doerf.zueriluft.BASE_URL
import li.doerf.zueriluft.BROADCAST_ACTION_FINISH_DOWNLOAD
import li.doerf.zueriluft.R
import li.doerf.zueriluft.ZueriluftApplication
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.db.daos.LocationDao
import li.doerf.zueriluft.db.daos.PollutantDao
import li.doerf.zueriluft.db.daos.ValueDao
import li.doerf.zueriluft.db.entities.Location
import li.doerf.zueriluft.db.entities.Pollutant
import li.doerf.zueriluft.db.entities.Value
import li.doerf.zueriluft.remote.ZLFLocation
import li.doerf.zueriluft.remote.ZLFValue
import li.doerf.zueriluft.remote.ZueriLuftFlowService
import li.doerf.zueriluft.util.asIsoString
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.toast
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*


class DownloadDataWorker constructor(
        val context: Context,
        params: WorkerParameters)
    : Worker(context, params) {

    companion object {
        private val LOGTAG = this::class.qualifiedName
        fun runNow() {
            val checker = OneTimeWorkRequest.Builder(DownloadDataWorker::class.java)
                    .build()
            WorkManager.getInstance().enqueue(checker)
        }
    }

    private var locationDao: LocationDao = AppDatabase.get(applicationContext).locationDao()
    private var pollutantDao: PollutantDao = AppDatabase.get(applicationContext).pollutantDao()
    private var valueDao: ValueDao = AppDatabase.get(applicationContext).valueDao()
    private var webSvc: ZueriLuftFlowService

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        webSvc = retrofit.create<ZueriLuftFlowService>(ZueriLuftFlowService::class.java)
    }

    override fun doWork(): Result {
        Log.d(LOGTAG, "onHandleWork")
        try {
            downloadLocations()
            downloadPollutants()
            downloadValues()
        } catch (e: Exception) {
            Log.e(LOGTAG, "caught exception while downloading data", e)
            return Result.failure()
        } finally {
            val localIntent = Intent(BROADCAST_ACTION_FINISH_DOWNLOAD)
            Log.d(LOGTAG, "sending BROADCAST_ACTION_FINISH_DOWNLOAD")
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(localIntent)

            val checker = OneTimeWorkRequest.Builder(CheckThresholdsWorker::class.java!!)
                    .build()
            WorkManager.getInstance().enqueue(checker)
        }
        return Result.success()
    }

    private fun downloadLocations() {
        try {
            Log.d(LOGTAG, "downloading locations")
            for (remoteLoc in webSvc.getLocations().execute().body()!!) {
                val locName = remoteLoc.name
                Log.d(LOGTAG, "location: $remoteLoc")
                val exLoc = locationDao.getByRemoteId(remoteLoc.id)
                if (exLoc != null) {
                    if (exLoc.latitude != null && exLoc.longitude != null) {
                        Log.d(LOGTAG, "exists and fully configured")
                        continue
                    }
                    updateLocation(exLoc, remoteLoc)
                    continue
                }
                createLocation(locName, remoteLoc)
            }
        } catch( e: Exception ) {
            Log.e(LOGTAG, "caught exception during location download", e)
            if ((applicationContext as ZueriluftApplication).isAppInForeground()) {
                applicationContext.runOnUiThread {
                    applicationContext.toast(applicationContext.getString(R.string.network_error))
                }
            }
        }
    }

    private fun createLocation(locName: String, remoteLoc: ZLFLocation) {
        val newLoc = Location(null, locName, remoteLoc.latitude, remoteLoc.longitude, remoteLoc.id)
        locationDao.insert(newLoc)
        Log.i(LOGTAG, "location created: $newLoc")
    }

    private fun updateLocation(exLoc: Location, remoteLoc: ZLFLocation) {
        Log.d(LOGTAG, "update existing location with lat/long")
        exLoc.latitude = remoteLoc.latitude
        exLoc.longitude = remoteLoc.longitude
        locationDao.update(exLoc)
        Log.d(LOGTAG, "existing location updated $exLoc")
    }

    private fun downloadPollutants() {
        Log.d(LOGTAG, "downloading pollutants")
        locationDao.getAll().forEach { location ->
            Log.d(LOGTAG, "downloading pollutants for location $location")
            try {
                for (poll in webSvc.getPollutants(location.remoteId).execute().body()!!) {
                    val pollName = poll.name
                    Log.d(LOGTAG, "location: ${location.name} pollutant: $poll")
                    if(pollutantDao.getByNameAndLocation(pollName, location.id!!).isNotEmpty()) {
                        Log.d(LOGTAG, "exists")
                        continue
                    }
                    val newPoll = Pollutant(null, pollName, poll.shortName, poll.unit, location.id!!, poll.id)
                    pollutantDao.insert(newPoll)
                    Log.i(LOGTAG, "pollutant created: $newPoll")
                }
            } catch( e: Exception ) {
                Log.e(LOGTAG, "caught exception during pollutant download", e)
                if ((applicationContext as ZueriluftApplication).isAppInForeground()) {
                    applicationContext.runOnUiThread {
                        applicationContext.toast(applicationContext.getString(R.string.network_error))
                    }
                }
            }
        }
    }

    private fun downloadValues() {
        Log.d(LOGTAG, "downloading values")
        locationDao.getAll().forEach { location ->
            Log.d(LOGTAG, "downloading latest values for location $location")
            var lastUpdateTime = getLastUpdateTime(location)
            try {
                if (lastUpdateTime == null) {
                    lastUpdateTime = yesterday()
                }
                for (remoteValue in webSvc.getAllValuesSince(location.remoteId, lastUpdateTime).execute().body()!!) {
                    if (!processRemoteValue(remoteValue, location)) continue
                }
            } catch( e: Exception ) {
                Log.e(LOGTAG, "caught exception during pollutant download", e)
                if ((applicationContext as ZueriluftApplication).isAppInForeground()) {
                    applicationContext.runOnUiThread {
                        applicationContext.toast(applicationContext.getString(R.string.network_error))
                    }
                }
            }
        }
    }

    private fun yesterday(): String {
        val y = Calendar.getInstance()
        y.add(Calendar.DAY_OF_YEAR, -1)
        val date = y.time
        return date.asIsoString()
    }

    private fun getLastUpdateTime(location: Location): String? {
        val pollutants = pollutantDao.getByLocation(location.id!!)
        if (pollutants.isEmpty()) {
            return null
        }
        val pollutant = pollutants.first()
        val value = valueDao.getLatestByPollutant(pollutant.id!!)
        if (value == null) {
            return null
        }
        return value.dateStr
    }

    private fun processRemoteValue(remoteValue: ZLFValue, location: Location): Boolean {
        Log.d(LOGTAG, "remoteValue: $remoteValue")
        val pollutant = getPollutant(remoteValue, location)
        val lastPersistedValue = valueDao.getByPollutantAndDate(pollutant.id!!, remoteValue.instant)

        if (lastPersistedValue != null && lastPersistedValue.dateStr == remoteValue.instant) {
            Log.d(LOGTAG, "value already exists")
            return false
        }

        val newValue = Value(null, remoteValue.value, remoteValue.instant, pollutant.id!!)
        valueDao.insert(newValue)
        Log.i(LOGTAG, "Value created: $newValue")
        return true
    }

    private fun getPollutant(remoteValue: ZLFValue, location: Location): Pollutant {
        val pollutants = pollutantDao.getByRemoteId(remoteValue.pollutantId!!)

        if (pollutants.isEmpty()) {
            throw IllegalArgumentException("pollutant with id ${remoteValue.pollutantId} not found")
        }

        return pollutants.first()
    }
}