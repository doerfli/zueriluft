package li.doerf.zueriluft.services

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import li.doerf.zueriluft.db.AppDatabase
import li.doerf.zueriluft.db.daos.LocationDao
import li.doerf.zueriluft.db.daos.PollutantDao
import li.doerf.zueriluft.db.daos.ThresholdDao
import li.doerf.zueriluft.db.daos.ValueDao
import li.doerf.zueriluft.db.entities.Location
import li.doerf.zueriluft.db.entities.Pollutant
import li.doerf.zueriluft.db.entities.Threshold
import li.doerf.zueriluft.db.entities.Value
import li.doerf.zueriluft.util.NotificationUtils
import li.doerf.zueriluft.util.PollutantLevel
import li.doerf.zueriluft.util.PollutantUtils
import org.jetbrains.anko.coroutines.experimental.bg


class CheckThresholdsWorker constructor(
        val context: Context,
        params: WorkerParameters)
    : Worker(context, params) {

    companion object {
        private val LOGTAG = this::class.qualifiedName
    }

    private var thresholdDao: ThresholdDao = AppDatabase.get(applicationContext).thresholdDao()
    private var locationDao: LocationDao = AppDatabase.get(applicationContext).locationDao()
    private var pollutantDao: PollutantDao = AppDatabase.get(applicationContext).pollutantDao()
    private var valueDao: ValueDao = AppDatabase.get(applicationContext).valueDao()

    override fun doWork(): Result {
        Log.d(LOGTAG, "onHandleWork")
        findExceeded()
        resetExceededThresholds()
        return Result.success()
    }

    private fun findExceeded() {
        Log.d(LOGTAG, "resolving thresholds that are enabled and not exceeded")
        val thresholdsNotExceeded = thresholdDao.getEnabledNotExceeded()
        val thresholdsExceedNew = mutableListOf<ThresholdExceeded>()

        thresholdsNotExceeded.forEach { threshold ->
            Log.d(LOGTAG, "$threshold")
            val thresholdLevel = PollutantLevel.valueOf(threshold.level)
            val pollutant = pollutantDao.getById(threshold.pollutantId)!!
            val location = locationDao.getById(threshold.locationId)!!
            val latestValue = valueDao.getLatestByPollutant(threshold.pollutantId) ?: throw IllegalStateException("no latest value found")
            val t = checkValueExceeded(pollutant, location, latestValue, thresholdLevel, threshold)
            if (t != null) {
                thresholdsExceedNew += t
            }
        }

        issueNotifications(thresholdsExceedNew)
    }

    private fun checkValueExceeded(pollutant: Pollutant, location: Location, latestValue: Value, thresholdLevel: PollutantLevel, threshold: Threshold): ThresholdExceeded? {
        val latestLevel = PollutantUtils.getLevelByPollutant(pollutant.name, latestValue.valueDbl)
        Log.d(LOGTAG, "latest value: $latestValue")

        if (latestLevel.isEqualOrHigher(thresholdLevel)) { // threshold exceeded
            Log.d(LOGTAG, "value exceeded threshold $latestLevel ($latestValue) >= $thresholdLevel")
            threshold.exceeded = true
            bg {
                thresholdDao.update(threshold)
            }



            return ThresholdExceeded(
                    threshold,
                    pollutant,
                    location,
                    latestLevel
            )
        }

        return null
    }

    private fun issueNotifications(thresholdsExceed: List<ThresholdExceeded>) {
        val groupNotification = thresholdsExceed.size > 1
        thresholdsExceed.forEach { threshold ->
            Log.d(LOGTAG, "issuing notification: $threshold")
            NotificationUtils.issueThresholdExceededNotification(applicationContext, threshold, groupNotification)
        }

        if (groupNotification) {
            NotificationUtils.issueThresholdExceededGroupNotification(applicationContext, thresholdsExceed)
        }
    }

    private fun resetExceededThresholds() {
        Log.d(LOGTAG, "checking if exceeded thresholds can be reset")
        val thresholdsExceeded = thresholdDao.getExceeded()

        thresholdsExceeded.forEach { threshold ->
            Log.d(LOGTAG, "$threshold")
            val thresholdLevel = PollutantLevel.valueOf(threshold.level)
            val pollutant = pollutantDao.getById(threshold.pollutantId)!!
            val latestValue = valueDao.getLatestByPollutant(threshold.pollutantId) ?: throw IllegalStateException("no latest value found")
            checkValueNoLongerExceeded(pollutant, latestValue, thresholdLevel, threshold)
        }
    }

    private fun checkValueNoLongerExceeded(pollutant: Pollutant, latestValue: Value, thresholdLevel: PollutantLevel, threshold: Threshold) {
        val latestLevel = PollutantUtils.getLevelByPollutant(pollutant.name, latestValue.valueDbl)
        Log.d(LOGTAG, "latest value: $latestValue")

        if (!latestLevel.isEqualOrHigher(thresholdLevel)) { // threshold no longer exceeded
            Log.d(LOGTAG, "threshold no longer exceeded $latestLevel ($latestValue) < $thresholdLevel")
            threshold.exceeded = false
            bg {
                thresholdDao.update(threshold)
            }
        }
    }

    data class ThresholdExceeded(val threshold: Threshold, val pollutant: Pollutant, val location: Location, val latestLevel: PollutantLevel)
}